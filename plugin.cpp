#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
{
	m_impl = new MusicPlayer(this, m_tracks);
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
		, {INTERFACE(IMusicPlayer), m_impl}
	},
	{
		{INTERFACE(IMusicPlayerTrackDataExtention), m_tracks},
		{INTERFACE(IMusicPlayerTrackProgressDataExtention), m_tracksProgress}
	}
	);

}

Plugin::~Plugin()
{
}

void Plugin::onReady()
{
	m_impl->init();
}
